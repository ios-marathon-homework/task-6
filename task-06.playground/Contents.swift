import UIKit
import Foundation

// Part 1

protocol Food {
    var title : String { get }
    func taste() -> String
}

protocol Storable {
    var name : String { get }
    var expired : Bool { get }
    var daysToExpire : Int { get }
}

class Vegetables : Food, Storable {
    var name: String
    var title: String {
        return name
    }
    
    var expired: Bool
    var daysToExpire : Int
    
    func taste() -> String {
        return (title + " tastes fresh")
    }
    
    init (name: String, expired: Bool, daysToExpire: Int) {
        self.name = name
        self.expired = expired
        self.daysToExpire = daysToExpire
    }
}

class Bread : Food {
    var name : String

    var title : String{
        return name
    }
    
    func taste() -> String {
        return (title + " tastes great")
    }
    
    init(name : String) {
        self.name = name
    }
}

class Meat : Food, Storable{
    var name : String
    var animal : String
    var count : Int
    var title : String {
        return name
    }
    
    var expired : Bool
    var daysToExpire: Int
    
    func taste() -> String {
        return (animal + " " + title + " tastes unhealthy")
    }

    init(name : String, animal : String, count : Int, expired : Bool, daysToExpire : Int) {
        self.name = name
        self.animal = animal
        self.count = count
        self.daysToExpire = daysToExpire
        self.expired = expired
    }
}

var sausages = Meat(name: "Sausages", animal: "Pig", count: 4, expired: false, daysToExpire: 14 )
var cucumber = Vegetables(name: "Chineese cucumber", expired: true, daysToExpire: 0)
var tomato = Vegetables(name: "Small tomatoes", expired: false, daysToExpire: 4)
var bread = Bread(name: "White bread")

var bagFromStore : [Food] = [sausages, cucumber, tomato, bread]
var fridge = [Storable]()

func sortBag(array : inout [Food]) -> [Food]{
    array.sort {$0.title < $1.title}
    return array
}

func printBag(array : [Food]) {
    for value in array {
        print(value.title)
    }
}

func tasteBag(array : [Food]) {
    for value in array {
        print(value.taste())
    }
}


func sortFridge(array : [Food]) -> [Storable]{
    for value in array {
        if let food = value as? Storable, !food.expired {
            fridge.append(food)
        }
    }
    
    fridge.sort (by: { a, b in
        if a.daysToExpire == b.daysToExpire {
            return a.name < b.name
        } else {
            return a.daysToExpire < b.daysToExpire
        }
    })
    return fridge
}

func printFridge(array : [Storable]) -> [Storable] {
    for value in array {
        print(value.name)
    }
    return fridge
}

sortBag(array: &bagFromStore)
print("# Items in the bag #")
printBag(array: bagFromStore)

print()
print("# Tasted items in the bag #")

tasteBag(array: bagFromStore)

sortFridge(array: bagFromStore)

print()
print("# Items in the fridge #")

printFridge(array: fridge)

// Part 2

protocol ConteinerLiFo : Collection{
    mutating func pop() -> Self.Iterator.Element
    mutating func push(element : Self.Iterator.Element)
}
protocol ConteinerFiFo : Collection{
    mutating func dequeue() -> Self.Iterator.Element
    mutating func enqueue(element : Self.Iterator.Element)
}
extension Array : ConteinerLiFo, ConteinerFiFo{
    mutating func pop() -> Self.Iterator.Element {
        let temp = self.popLast()
        return temp!
    }
    mutating func push(element: Self.Iterator.Element) {
        self.append(element)
    }
    mutating func dequeue() -> Self.Iterator.Element {
        let temp = self.popLast()
        return temp!
    }
    mutating func enqueue(element: Self.Iterator.Element){
        self.insert(element, at: 0)
    }

}
var t = [1,2,3,4]
t.pop()

t.push(element: 5)


t.insert(1, at: 0)

t.enqueue(element: 0)

t.dequeue()
t

